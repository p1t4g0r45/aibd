#!/bin/bash
yum install -y vsftpd #instalacja vsftpd
chkconfig --add vsftpd #dodanie do autostartu
chkconfig vsftpd on
service vsftpd start #startowanie
echo -e "n\np\n1\n1\n+4G\nw" | fdisk /dev/sdb
#powyzej dodanie partycji
mkfs.ext4 /dev/sdb1 #stworzenie systemu plikow ext4 na partycji
mkdir /backups #stworzenie katalogu do ktorego zamontujemy partycje
mount /dev/sdb1 /backups #podmontowanie partycji
cat > /root/logins.txt <<END
backup
backup
END
#powyzej dodanie pliku z loginami (baza wirtualnych userow)
db_load -T -t hash -f /root/logins.txt /etc/vsftpd/login.db # wczytanie pliku z loginami do bazy danych   userow
chmod 600 /etc/vsftpd/login.db #wykoszenie uprawnien dla innych dla pliku z loginami
cat > /etc/pam.d/ftp <<END
auth required /lib64/security/pam_userdb.so db=/etc/vsftpd/login
account required /lib64/security/pam_userdb.so db=/etc/vsftpd/login
END
#powyzej konfiguracja PAM dla vsftpd
useradd -d /backups virtual #dodanie uzytkownika virtual z home dir /backups
cp /etc/hosts /backups #skopiowanie jakiegos pliku na backups
chown -R virtual:virtual /backups #nadanie uprawnien do /backups dla usera virtual
mv /etc/vsftpd/vsftpd.conf /root/vsftpd.conf.backup #backup pliku vsftpd.conf
cat > /etc/vsftpd/vsftpd.conf <<END
anonymous_enable=NO
local_enable=YES
write_enable=YES
anon_upload_enable=YES
anon_mkdir_write_enable=YES
anon_other_write_enable=YES
anon_world_readable_only=NO
chroot_local_user=YES
guest_enable=YES
guest_username=virtual
listen=YES
listen_port=10021
pasv_min_port=30000
pasv_max_port=30999
END
#powyzej konfiguracja dla vsftpd, tak zeby uzywal usera virtual
service vsftpd restart #restart vsftpd (reload konfiguracji)
