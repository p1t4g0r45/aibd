#!/bin/bash
EXT=dar
NAME=wazne_dane-`date +'%Y-%m-%d'`
lftp -u backup,backup "ADRES_SERWERA:10021" <<END
cls -1 "$NAME"* |awk -F\".\" \'{print \$(NF-1)}\'|sort -n|tail -n1 > "$NAME"_last_no.txt
END
LAST_NO=`cat "$NAME"_last_no.txt`
rm -fr "$NAME"_last_no.txt
LAST_FILE_NAME="$NAME.$LAST_NO.$EXT"
lftp -u backup,backup "ADRES_SERWERA:10021" <<END 
set xfer:clobber yes
get "$LAST_FILE_NAME"
END
dar -x "$NAME" -R /wazne_dane -E "lftp -u backup,backup "ADRES_SERWERA:10021" -e \"set xfer:clobber yes; get %b.%n.%e; exit\"; find . |grep $NAME|grep -v $LAST_FILE_NAME|grep -v %b.%n.%e|xargs rm -fr"
rm -fr $LAST_FILE_NAME